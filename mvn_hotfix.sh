#!/bin/bash

. ./mvn_functions.sh


ver=$(getVer)
ver=$(zeroT $ver)
devVer=$(incZ $ver)
releaseVer=$(rmSNAPSHOT $devVer)

mvn release:prepare -DdevelopmentVersion=$devVer -DreleaseVersion=$releaseVer -Dtag=v$releaseVer
mvn release:perform
