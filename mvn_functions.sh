#!/bin/bash

function getVer {
    ver=$(xmlstarlet sel -T -t -v "/_:project/_:version" pom.xml)
    echo $ver
}

function incY {
    version=$1
    X=$(echo $version | cut -d . -f 1)
    Y=$(echo $version | cut -d . -f 2)
    Z=$(echo $version | cut -d . -f 3)
    T=$(echo $version | cut -d . -f 4)

    Y=$(echo $Y + 1 | bc)

    echo "$X.$Y.$Z.$T"
}

function incZ {
    version=$1
    X=$(echo $version | cut -d . -f 1)
    Y=$(echo $version | cut -d . -f 2)
    Z=$(echo $version | cut -d . -f 3)
    T=$(echo $version | cut -d . -f 4)

    Z=$(echo $Z + 1 | bc)

    echo "$X.$Y.$Z.$T"
}

function zeroT {
    version=$1
    X=$(echo $version | cut -d . -f 1)
    Y=$(echo $version | cut -d . -f 2)
    Z=$(echo $version | cut -d . -f 3)
    S=$(echo $version | cut -d - -f 2 -s)

    if [ -n "$S" ]; then
        S="-$S"
    fi

    echo "$X.$Y.$Z.0$S"
}

function zeroZ {
    version=$1
    X=$(echo $version | cut -d . -f 1)
    Y=$(echo $version | cut -d . -f 2)
    Z=$(echo $version | cut -d . -f 3)
    S=$(echo $version | cut -d - -f 2 -s)

    if [ -n "$S" ]; then
        S="-$S"
    fi

    echo "$X.$Y.0.0$S"
}

function rmSNAPSHOT {
    version=$1
    echo $version | cut -d - -f 1
}

function branchName {
    version=$1
    version=$(rmSNAPSHOT $version)
    version=$(zeroT $version)
    version=$(zeroZ $version)
    echo $version
}

function devVer {
    version=$1
    version=$(incY $version)
    version=$(zeroT $version)
    echo $version
}

