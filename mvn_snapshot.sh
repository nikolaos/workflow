#!/bin/sh

git pull
mvn -B release:update-versions
git commit -a -m "Rollup version"
git push
mvn deploy
