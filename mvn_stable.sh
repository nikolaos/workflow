#!/bin/bash

. ./mvn_functions.sh

ver=$(getVer)

branchDevVer=$(zeroT $(zeroZ $ver) )
branchReleaseVer=$(rmSNAPSHOT $branchDevVer)

branch=$(rmSNAPSHOT $branchDevVer)
devVer=$(devVer $ver)




echo Brach = $branch
echo DevVer = $devVer
echo Branch Dev Ver = $branchDevVer
echo Branch Release Ver = $branchReleaseVer


mvn release:branch -DbranchName=$branch -DdevelopmentVersion=$devVer

git pull
git checkout $branch

mvn release:prepare -DdevelopmentVersion=$branchDevVer -DreleaseVersion=$branchReleaseVer -Dtag=v$branchReleaseVer
mvn release:perform

git checkout master